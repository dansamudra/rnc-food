import React, { useState } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import SearchBar from '../components/SearchBar';
import useResult from '../hooks/useResult'
import ResultList from '../components/ResultList'

const SearchScreen = () => {
    const [term, setTerm] = useState('');
    const [searchApi, result, errMessage] = useResult()

    const filterResultByPrice = (price) => {
        //price==='$'||'$$'||'$$$'
        return result.filter(result => {
            return result.price === price;
        });
    };

    console.log(result)
    return (
        <>
            <SearchBar
                term={term}
                onTermChange={setTerm}
                onTermSubmit={() => searchApi(term)}
            />
            {/* newTerm is used for search new term in searchbar,
            onTermChange is named free, so onTermChange is set to a newTerm..
            ..when user type a text*/}
            {errMessage ? <Text>{errMessage}</Text> : null}
            <ScrollView style={styles.ScrollContainer}>
                <ResultList
                    result={filterResultByPrice('$')}
                    title='Cost Effective' />
                <ResultList
                    result={filterResultByPrice('$$')}
                    title='Bit Pricier' />
                <ResultList
                    result={filterResultByPrice('$$$')}
                    title='Big Spender' />
            </ScrollView>
        </>
    )
};

const styles = StyleSheet.create({
    ScrollContainer: {
        flex: 1,
    }
});

export default SearchScreen;