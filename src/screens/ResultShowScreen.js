import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, Image } from 'react-native';
import yelp from '../API/yelp';
import { FlatList } from 'react-native-gesture-handler';

const ResultShowScreen = ({ navigation }) => {
    const [result, setResult] = useState(null)
    //null mean we do not yet fetch any data in array
    const id = navigation.getParam('id')
   
    const getResult = async id => {
        const response = await yelp.get(`/${id}`);
        setResult(response.data);
    };
    useEffect(() => {
        getResult(id)
    }, [])
    //useEffect to run function exactly one time, provided argument that we give a 2nd argument in empty array

    if (!result){
        return null;
        //this mean do not show until get result/not null 
    }

    return (
        <View>
            <Text>
                {result.name}
            </Text>
            <FlatList
            data ={result.photos}
            keyExtractor={(photo)=>photo}
            renderItem={({item})=>{
                return(
                    <Image 
                    source={{uri:item}}
                    style={styles.imageContainer}
                    />
                )
            }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    imageContainer:{
        height:200,
        width:300
    }
})

export default ResultShowScreen