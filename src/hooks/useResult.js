import {useState, useEffect} from 'react'
import yelp from '../API/yelp'

export default ()=>{
    const [result, setResult] = useState([]);
    const [errMessage, setErrMessage] = useState('');

    const searchApi = async searchTerm => {
        try {
            const response = await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: searchTerm,
                    location: 'san jose'
                }
            });
            //above is how to access YELP API
            setResult(response.data.businesses);
            //response.data.businesses is going to call response body in https://www.yelp.com/developers/documentation/v3/business array in businesses
        } catch (err) {
            setErrMessage('Something went wrong')
        }
    };
    useEffect(()=>{
        searchApi('pasta');
    },[]);
    return [searchApi, result, errMessage]
};