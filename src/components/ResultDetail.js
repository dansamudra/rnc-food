import React from 'react'
import {View, Image, StyleSheet, Text, ScrollView}from 'react-native'

const ResultDetail =({results})=>{
    //results is a business object
    return (
        <View style={styles.container}>
            <Image style={styles.imageStyle} source={{uri: results.image_url}}/>
            {/* there two curly braces,
            the outer mean we're about to refer JS expression
            the inner one is for forming the actual object we want to pass */}
            <Text style={styles.nameStyle}>
                {results.name}
            </Text>
            <Text style={styles.nameStyle}>
                {results.rating} Stars, {results.review_count}
            </Text>
        </View>
    )
}

const styles=StyleSheet.create({
    container:{
        marginLeft:15,
        flex:1
    },
    imageStyle:{
        width:250,
        height: 120,
        borderRadius:4,
        marginBottom: 5,
    },
    nameStyle:{
        fontWeight:"bold",
    }
})

export default ResultDetail