import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import ResultDetail from './ResultDetail'
import { withNavigation } from 'react-navigation';

const ResultList = ({ title, result, navigation }) => {
    
    if(!result.length){
        return (null)
    }
    
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
            <Text style={{ marginLeft: 15 }}>Result:{result.length}</Text>
            <FlatList
                horizontal //didnt need to ={true} because just prop name mean true
                showsHorizontalScrollIndicator={false}
                data={result}
                keyExtractor={(result) => result.id}//that id mean rerendering from database yelp with key id
                renderItem={({ item }) => {
                    return (
                        <TouchableOpacity onPress={()=>navigation.navigate('ResultShow',{id:item.id})}>
                            <ResultDetail results={item} />
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
};

const styles = StyleSheet.create({
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 15,
        marginBottom: 5
    },
    container: {
        marginBottom: 10,
        flex: 1
    }
});

export default withNavigation(ResultList);